#ifndef STREAM9_TEST_SCOPED_ENV_HPP
#define STREAM9_TEST_SCOPED_ENV_HPP

#include <cassert>

#include <stdlib.h>

#include <stream9/cstring_ptr.hpp>

namespace stream9::test {

class scoped_env
{
public:
    scoped_env(cstring_ptr const& name,
               cstring_ptr const& value)
        : m_name { name }
    {
        assert(m_name);

        m_old_value = ::getenv(m_name);

        ::setenv(m_name, value.c_str(), 1);
    }

    scoped_env(cstring_ptr const& name)
        : m_name { name }
    {
        assert(m_name);

        m_old_value = ::getenv(m_name);

        ::unsetenv(m_name);
    }

    ~scoped_env()
    {
        if (m_old_value) {
            ::setenv(m_name, m_old_value, 1);
        }
        else {
            ::unsetenv(m_name);
        }
    }

private:
    char const* m_name; // non-null
    char const* m_old_value = nullptr;
};

} // namespace testing

#endif // STREAM9_TEST_SCOPED_ENV_HPP
