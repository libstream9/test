#ifndef STREAM9_TEST_OSTREAM_CAPTUREER_HPP
#define STREAM9_TEST_OSTREAM_CAPTUREER_HPP

#include <ostream>

namespace stream9::test {

template<typename CharT, typename Traits>
class ostream_captureer
{
public:
    ostream_captureer(std::basic_ostream<CharT, Traits>& st)
        : m_stream { &st }
    {
        m_orig_buf = st.rdbuf();
        st.rdbuf(&m_buf);
    }

    ~ostream_captureer() noexcept
    {
        m_stream->rdbuf(m_orig_buf);
    }

    auto str() const
    {
        return m_buf.str();
    }

private:
    std::basic_ostream<CharT, Traits>* m_stream; // non-null
    std::stringbuf m_buf;
    std::basic_streambuf<CharT, Traits>* m_orig_buf;
};

} // namespace stream9::test

#endif // STREAM9_TEST_OSTREAM_CAPTUREER_HPP
