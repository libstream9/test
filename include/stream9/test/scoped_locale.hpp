#ifndef STREAM9_TEST_SCOPED_LOCALE_HPP
#define STREAM9_TEST_SCOPED_LOCALE_HPP

#include <clocale>

namespace stream9::test {

class scoped_locale
{
public:
    scoped_locale(int const category, char const* const name)
        : m_category { category }
    {
        m_old_name = std::setlocale(m_category, name);
    }

    ~scoped_locale() noexcept
    {
        std::setlocale(m_category, m_old_name);
    }

private:
    int m_category;
    char const* m_old_name;
};

} // namespace stream9::test


#endif // STREAM9_TEST_SCOPED_LOCALE_HPP
