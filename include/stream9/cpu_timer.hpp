#ifndef STREAM9_CPU_TIMER_HPP
#define STREAM9_CPU_TIMER_HPP

#include <chrono>

namespace stream9 {

class cpu_timer
{
public:
    using clock_t = std::chrono::system_clock;

public:
    cpu_timer() noexcept
    {
        start();
    }

    void start() noexcept
    {
        m_time = clock_t::now();
    }

    clock_t::duration
    elapsed() noexcept
    {
        auto now = clock_t::now();
        auto n = now - m_time;
        m_time = now;
        return n;
    }

private:
    clock_t::time_point m_time;
};

} // namespace stream9

#endif // STREAM9_CPU_TIMER_HPP
